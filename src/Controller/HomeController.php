<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PhoneService;
use Symfony\Component\HttpClient\HttpClient;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {

        $serviceRepo = $this->getDoctrine()->getRepository(PhoneService::class);
        $services = $serviceRepo->findAll();

        $client = HttpClient::create();
        $response = $client->request('GET','http://127.0.0.1:8001/api/contacts.json');
        $contact_information = $response->toArray();
        
        return $this->render('home/index.html.twig', [
            'services' => $services,
            'contact' => $contact_information,
        ]);
    }
}
