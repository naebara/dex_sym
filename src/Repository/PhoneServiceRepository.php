<?php

namespace App\Repository;

use App\Entity\PhoneService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PhoneService|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneService|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneService[]    findAll()
 * @method PhoneService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneService::class);
    }

    // /**
    //  * @return PhoneService[] Returns an array of PhoneService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneService
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
