

import '../styles/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
 import $ from 'jquery';

 require('bootstrap');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');


import mapsImage from '../images/maps.jpg';

var mapsImg = document.getElementById('maps');
mapsImg.src = mapsImage;


import phone1Image from '../images/phone1.png';

var phone1Img = document.getElementById('phone1');
phone1Img.src = phone1Image;

import phone2Image from '../images/phone2.png';

var phone2Img = document.getElementById('phone2');
phone2Img.src = phone2Image;

import phone3Image from '../images/phone3.png';

var phone3Img = document.getElementById('phone3');
phone3Img.src = phone3Image;

import aboutImage from '../images/about.png';

var aboutImg = document.getElementById('aboutImage');
aboutImg.src = aboutImage;






$( document ).ready(function() {
    
    $("#more").on("click", function(){
        if($(this).text() === "Sloganul nostru")
        {
            $("#a1").removeClass("d-none");
            $("#a3, #a4").addClass("d-none");
            $(this).text("Inapoi");
        }
        else
        {
            $("#a3, #a4").removeClass("d-none");
            $("#a1").addClass("d-none");
            $(this).text("Sloganul nostru"); 
        }
       
    })
});